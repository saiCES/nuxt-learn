
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: "https://fonts.googleapis.com/css?family=Roboto&display=swap" }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color:'#ff0000', height:'4px',duration:8000,continuous:true },
  /*
  ** Global CSS
  */
  css: [
    '~assets/styles/main.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~plugins/core-components.js',
    '~plugins/date-filter.js',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
  ],
  axios: {
    baseURL : process.env.BASE_URL || 'https://nuxt-blog-e91df.firebaseio.com',
    credentials: false,
    proxyHeaders: false,
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  env: {
    baseUrl: process.env.BASE_URL || 'https://nuxt-blog-e91df.firebaseio.com',
    fbAPIKey : 'AIzaSyD-HdOIb4Bhnd2jkLoRnTNFiZJju_815tc'
  },
  pageTransition: {
    name: 'fade',
    mode: 'out-in'
  }
}
