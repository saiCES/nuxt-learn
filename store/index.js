import Vuex from "vuex";

const createStore = () => {
  return new Vuex.Store({
    state: {
      loadedPosts: [],
      token: null
    },
    mutations: {
      setPosts(state, posts) {
        state.loadedPosts = posts;
      },
      addPost(state, post) {
        state.loadedPosts.push(post);
      },
      editPost(state, editedPost) {
        const postIndex = state.loadedPosts.findIndex(post => post.id === editedPost.id)
        state.loadedPosts[postIndex] = editedPost
      },
      setToken(state, token) {
        state.token = token
      },
      clearToken(state, token) {
        state.token = null
      }
    },
    actions: {
      nuxtServerInit(vuexContext, context) {
        return context.app.$axios.$get('/post.json')
          .then(data => {
            const postsArray = []
            for (const key in data) {
              postsArray.push({ ...data[key], id: key })
            }
            vuexContext.commit('setPosts', postsArray)
          })
          .catch(e => context.error(e));
      },
      addPost(vuexContext, post) {
        const createdPost = {
          ...post,
          updatedDate: new Date()
        }
        return this.$axios.$post(process.env.baseUrl + '/post.json?auth=' + vuexContext.state.token, createdPost)
          .then((res) => {
            vuexContext.commit("addPost", { ...createdPost, id: res.data.name })
          })
          .catch(
            error => console.log(error)
          )

      },
      editPost(vuexContext, editedPost) {
        return this.$axios
          .$put(process.env.baseUrl + "/post/" +
            editedPost.id +
            ".json?auth=" + vuexContext.state.token,
            editedPost
          )
          .then(res => {
            vuexContext.commit("editPost", editedPost)
          })
          .catch(e => console.log(e));
      },
      setPosts(vuexContext, posts) {
        vuexContext.commit("setPosts", posts);
      },
      authenticateUser(vuexContext, authData) {
        let authUrl = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" + process.env.fbAPIKey
        if (!authData.isLogin) {
          authUrl = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=" + process.env.fbAPIKey

        }
        return this.$axios.$post(authUrl, {
          email: authData.email,
          password: authData.password,
          returnSecureToken: true
        }).then(data => {
          vuexContext.commit("setToken", data.idToken)
          localStorage.setItem('token',data.idToken)
          localStorage.setItem('expiryIn',new Date().getTime()+data.expiresIn)
          vuexContext.commit("setLogoutTimer",data.expiresIn * 1000)
        })
          .catch(e => console.log(e))
      },
      setLogoutTimer(vuexContext, duration) {
        setTimeout(() => {
          vuexContext.commit("cleatToken")
        }, duration)
      },
      initAuth(vuexContext){
        const token = localStorage.getItem("token")
        const expiryDate = localStorage.getItem("expiryIn")

        if(new Date() > expiryDate || !token){
          return;
        }
        else{
          vuexContext.commit("setToken",token)
        }
      }
    },
    getters: {
      loadedPosts(state) {
        return state.loadedPosts;
      },
      isAuthenticated(state) {
        return state.token != null
      }
    }
  });
};




export default createStore